<?php

namespace App\Http\Controllers;

use App\News;
use App\Project;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        $user = Auth::user();

        $posts = News::all();
        $projects = Project::where('user_id', $user_id)->take(3)->orderBy('created_at', 'desc')->get();

        return view('dashboard', compact('projects', 'posts', 'user_id', 'user'));
    }
}
