<?php

namespace App\Http\Controllers;

use App\Policies\UserPolicy;
use Illuminate\Http\Request;
use \App\News;
use \App\Comment;
use PhpParser\Node\Expr\PostDec;
use Illuminate\Support\Facades\Auth;

class NewsController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $posts = News::all();

        return view('news.index', compact('posts', 'user'));
    }

    public function show($id)
    {
        $user = Auth::user();
//        $comments = Comment::all();
        $post = News::findOrFail($id);

        return view('news.show', compact('post', 'user'));
    }

    public function create()
    {
        $user = Auth::user();
        $this->authorize('isAdmin', $user);

        return view('news.create', compact('user'));
    }


    public function edit($id)
    {
        $user = Auth::user();
        $this->authorize('isAdmin', $user);

        $post = News::findOrFail($id);
        return view('news.edit', compact('post', 'user'));
    }

    public function update($id)
    {
        request()->validate([
            'title' => 'required',
            'description' => 'required',
            'body' => 'required'
        ]);

        $posts = News::findOrFail($id);

        $posts->title = request('title');
        $posts->description = request('description');
        $posts->body = request('body');

        $posts->save();

        return redirect(route('show_news', $id));
    }

    public function destroy($id)
    {
        $posts = News::findOrFail($id);
        $posts->delete();

        return redirect(route('news'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'title' => 'required',
            'description' => 'required',
            'body' => 'required'
        ]);

        $logoName = time() . '.' . request()->cover_image->getClientOriginalName();

        $post = new News();
        $post->title = request('title');
        $post->description = request('description');
        $post->body = request('body');
        $post->user_id = Auth::user()->id;
        $post->cover_image = $logoName;

        $post->save();

        request()->cover_image->move(public_path('storage/news/' . $post->id), $logoName);

        return redirect(route('news'));
    }

}
