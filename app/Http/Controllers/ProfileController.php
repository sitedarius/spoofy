<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileRequest;
use App\Http\Requests\PasswordRequest;
use App\Project;
use App\User;
use Illuminate\Support\Facades\Hash;
use \Illuminate\Http\Request;
use App\Http\Controllers\Auth;

class ProfileController extends Controller
{
    /**
     * Show the form for editing the profile.
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        return view('profile.edit', compact('user'));
    }

    public function update($id)
    {
        request()->validate([
            'email' => 'required',
            'name' => 'required'
        ]);

        $user = User::findOrFail($id);

        $imageName = time() . '.' . request()->cover_image->getClientOriginalName();

        $user->name = request('name');
        $user->email = request('email');
//        $user->phone = request('phone');
//        $user->website = request('website');
        $user->image = $imageName;

        request()->cover_image->move(public_path('storage/users/' . $user->id), $imageName);

        $user->save();

        return back()->withStatus(__('Profile successfully updated.'));
    }

    /**
     * Change the password
     *
     * @param \App\Http\Requests\PasswordRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function password(PasswordRequest $request)
    {
        auth()->user()->update(['password' => Hash::make($request->get('password'))]);

        return back()->withPasswordStatus(__('Password successfully updated.'));
    }
}
