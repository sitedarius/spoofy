<?php

return [
//    Projects
    'edit' => 'Edit',
    'tasks' => 'Tasks:',
    'tasks_empty' => 'You dont have any tasks...',
    'status_empty' => 'Private',
    'status' => 'Shared',
    'created_at' => 'Created at:',
    'image' => 'Image:',
    'edit_page' => 'Edit a project:',
];
