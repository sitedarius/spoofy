<?php

return [
//    News
    'read_more' => 'Read more...',
    'comments' => 'Comments:',
    'author' => 'Author:',
    'by' => 'By:',
    'updated' => 'Updated on:',
    'no_comments' => 'There are no comments...',
    'edit' => 'Edit',
];
