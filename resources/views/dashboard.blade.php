@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
    @include('layouts.headers.cards')

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-9"><h1>News</h1></div>
            @foreach($posts->slice(0, 3) as $post)
                <div class="col-md-4 mt-2">
                    <a href="{{ route('show.news', [$post->id]) }}">
                        <div class="card card-project" style="border-top: none;">
                            <img src="/storage/projects/{{ $post->id }}/{{ $post->cover_image }}"
                                 class="card-img-top" alt="{{ $post->cover_image }}">
                            <div class="card-body">
                                <h5 class="card-title mb-0">{{ $post->title }}</h5>
                                <hr class="my-1">
                                <p class="card-text">{{ $post->description }}</p>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
            <div class="col-md-12 text-center mt-4"><a href="#">View All...</a></div>

            <div class="col-9"><h1>Projects</h1></div>
            @foreach($projects as $project)
                <div class="col-md-4 mt-2">
                    <a href="{{ route('show.project', [$project->id]) }}">
                        <div class="card card-project" style="border-top: none;">
                            <img src="/storage/users/{{ $user_id }}/projects/{{ $project->id }}/{{ $project->cover_image }}"
                                 class="card-img-top" alt="{{ $project->cover_image }}">
                            <div class="card-body">
                                <h5 class="card-title mb-0">{{ $project->name }}</h5>
                                <hr class="my-1">
                                <p class="card-text">{{ $project->description_short }}</p>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
            <div class="col-md-12 text-center mt-4"><a href="{{ route('projects') }}">View All...</a></div>

            <div class="col-9"><h1>Agenda</h1></div>
            <div class="col-md-4 mt-2">
                <div class="card">
                    <div class="card-body">
                        <br><br><br><br><br><br><br><br>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mt-2">
                <div class="card">
                    <div class="card-body">
                        <br><br><br><br><br><br><br><br>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mt-2">
                <div class="card">
                    <div class="card-body">
                        <br><br><br><br><br><br><br><br>
                    </div>
                </div>
            </div>
            <div class="col-md-12 text-center mt-4"><a href="#">View All...</a></div>

        </div>
        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush
