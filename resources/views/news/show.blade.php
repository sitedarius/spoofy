@extends('layouts.app')

@section('title', $post->title)

@section('content')
    @include('layouts.headers.cards')

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-10 col-9">
                                <h2 class="mb-0">{{ $post->title }}</h2>
                            </div>
                            <div class="col-md-2 col-3 text-left">
                                <a class="btn btn-link" href="{{ route('edit.news', [$post->id]) }}">
                                    <small>{{ __('projects.edit') }}</small>
                                </a>
                            </div>
                            <div class="col-12">
                                <hr class="my-1">
                                <small>{{ Carbon\Carbon::parse($post->created_at)->format('j F Y') }}</small><br>
                                <small>Laatse update: {{ Carbon\Carbon::parse($post->updated_at)->format('j F Y') }}</small>
                                <hr class="my-1">
                                <p>{!! $post->body !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
