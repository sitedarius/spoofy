@extends('layouts.app')

@section('title', 'News')

@section('content')
    @include('layouts.headers.cards')

    <div class="container-fluid mt--7">
        <div class="row">
            {{--            <div class="col-md-6 col-6 text-right">--}}
            {{--                <a href="{{ route('create.project') }}" class="btn btn-link"><i--}}
            {{--                        class="fas fa-plus"></i></a>--}}
            {{--            </div>--}}
            @if (count($posts))
                @foreach($posts as $post)
                    <div class="col-md-4 mt-4">
                        <a href="{{ route('show.news', [$post->id]) }}">
                            <div class="card card-project" style="border-top: none;">
                                <img src="/storage/projects/{{ $post->id }}/{{ $post->cover_image }}"
                                     class="card-img-top" alt="{{ $post->cover_image }}">
                                <div class="card-body">
                                    <h5 class="card-title">{{ $post->title }}</h5>
                                    <hr>
                                    <p class="card-text">{{ $post->description }}</p>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            @else
                <div class="col-md-1"></div>
                <div class="col-md-10 mt-5 justify-content-center text-center">
                    <img src="/storage/nothing/nothing.png" class="w-25">
                    <h3 class="mt-5 pt-5">{{ __('messages.empty') }}</h3>
                </div>
                <div class="col-md-1"></div>
            @endif
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush
