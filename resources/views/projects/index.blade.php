@extends('layouts.app')

@section('title', 'Projecten')

@section('content')
    @include('layouts.headers.cards')

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-md-12 col-6 text-right">
                <a href="{{ route('create.project') }}" class="btn btn-link"><i
                        class="fas fa-plus"></i></a>
            </div>
            @if (count($projects))
                @foreach($projects as $project)
                    <div class="col-md-4 mt-4">
                        <a href="{{ route('show.project', [$project->id]) }}">
                            <div class="card card-project" style="border-top: none;">
                                <img src="/storage/users/{{ Auth::user()->id }}/projects/{{ $project->id }}/{{ $project->cover_image }}"
                                     class="card-img-top" alt="{{ $project->cover_image }}">
                                <div class="card-body">
                                    <h5 class="card-title">{{ $project->name }}</h5>
                                    <hr>
                                    <p class="card-text">{{ $project->description_short }}</p>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            @else
                <div class="col-md-1"></div>
                <div class="col-md-10 mt-5 justify-content-center text-center">
                    <img src="/storage/nothing/nothing.png" class="w-25">
                    <h3 class="mt-5 pt-5">{{ __('messages.empty') }}</h3>
                </div>
                <div class="col-md-1"></div>
            @endif
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush
