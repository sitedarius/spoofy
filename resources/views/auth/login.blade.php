@extends('layouts.app', ['class' => 'bg-dark'])

@section('content')
    @include('layouts.headers.guest')

    <div class="container mt--8 pb-5">
        <div class="row">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <div class="card card-signin mt-3">
                    <div class="card-body">
                        <h3 class="card-title text-center">Sign In</h3>
                        <hr class="mt-2 mb-3">
                        <form class="form-signin" role="form" method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="form-label-group">
                                <label for="inputEmail" class="mb-0 font-weight-bold">Email address</label>
                                <input type="email" id="inputEmail"
                                       class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}"
                                       name="email" placeholder="Email address" required autofocus>
                            </div>

                            <div class="form-label-group">
                                <label for="inputPassword" class="mb-0 mt-2 font-weight-bold">Password</label>
                                <input type="password" id="inputPassword"
                                       class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                       name="password" placeholder="Password"
                                       required>
                            </div>

                            <div class="custom-control custom-checkbox mb-3 mt-3">
                                <input type="checkbox" class="custom-control-input" id="customCheck1">
                                <label class="custom-control-label" for="customCheck1" name="remember">Remember
                                    password</label>
                            </div>
                            <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Sign in
                            </button>
                            <hr class="my-4">
                            <button class="btn btn-lg btn-apple btn-block text-uppercase" type="submit"><i
                                    class="fab fa-apple mr-2"></i> Sign in with Apple
                            </button>
                            <button class="btn btn-lg btn-google btn-block text-uppercase" type="submit"><i
                                    class="fab fa-google mr-2"></i> Sign in with Google
                            </button>
                            <button class="btn btn-lg btn-facebook btn-block text-uppercase" type="submit"><i
                                    class="fab fa-facebook-f mr-2"></i> Sign in with Facebook
                            </button>
                        </form>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-6">
                        @if (Route::has('password.request'))
                            <a href="{{ route('password.request') }}" class="text-light">
                                <small>{{ __('Forgot password?') }}</small>
                            </a>
                        @endif
                    </div>
                    <div class="col-6 text-right">
                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="text-light">
                                <small>{{ __('Create an account') }}</small>
                            </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
