@extends('layouts.app', ['class' => 'bg-dark'])

@section('content')
    @include('layouts.headers.guest')

    <div class="container mt--8 pb-5">
        <!-- Table -->
        <div class="row justify-content-center">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <div class="card card-signin mt-3">
                    <div class="card-body">
                        <h3 class="card-title text-center">Sign Up</h3>
                        <hr class="mt-2 mb-3">
                        <form class="form-signin" role="form" method="POST" action="{{ route('register') }}">
                            @csrf
                            <div class="form-label-group {{ $errors->has('name') ? ' has-danger' : '' }}">
                                <label for="inputEmail" class="mb-0 font-weight-bold">Name</label>
                                <input type="text" id="inputEmail"
                                       class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name"
                                       value="{{ old('name') }}" required autofocus>
                            </div>
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif

                            <div class="form-label-group {{ $errors->has('email') ? ' has-danger' : '' }}">
                                <label for="inputEmail" class="mb-0 font-weight-bold">Email</label>
                                <input type="email" id="inputEmail"
                                       class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}"
                                       name="email" value="{{ old('email') }}"
                                       required>
                            </div>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif

                            <div class="form-label-group">
                                <label for="inputPassword" class="mb-0 mt-2 font-weight-bold">Password</label>
                                <input type="password" id="inputPassword"
                                       class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                       name="password" placeholder="Password"
                                       required>
                            </div>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif

                            <div class="form-label-group">
                                <label for="inputPassword" class="mb-0 mt-2 font-weight-bold">Confirm password</label>
                                <input type="password" id="inputPassword"
                                       class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                       type="password"
                                       name="password_confirmation" required>
                            </div>




                            <button class="btn btn-lg btn-primary btn-block text-uppercase mt-2" type="submit">Sign up
                            </button>
                            <hr class="my-4">
                            <button class="btn btn-lg btn-apple btn-block text-uppercase" type="submit"><i
                                    class="fab fa-apple mr-2"></i> Sign up with Apple
                            </button>
                            <button class="btn btn-lg btn-google btn-block text-uppercase" type="submit"><i
                                    class="fab fa-google mr-2"></i> Sign up with Google
                            </button>
                            <button class="btn btn-lg btn-facebook btn-block text-uppercase" type="submit"><i
                                    class="fab fa-facebook-f mr-2"></i> Sign up with Facebook
                            </button>
                        </form>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-12 text-center">
                        @if (Route::has('login'))
                            <a href="{{ route('login') }}" class="text-light">
                                <small>{{ __('Already have an account, sign in') }}</small>
                            </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
