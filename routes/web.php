<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('locale/{locale}', function ($locale){
    Session::put('locale', $locale);
    return redirect()->back();
});

Route::get('/', function () {
    return view('auth.login');
});
Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('home');

Route::get('/project/{id}', 'ProjectsController@show')->name('show.project');
Route::get('/p/create', ['middleware' => 'auth', 'uses' => 'ProjectsController@create'])->name('create.project');
Route::get('/project/{project}/edit', 'ProjectsController@edit')->name('edit.project');
Route::get('/projects', 'ProjectsController@index')->name('projects');
Route::post('/projects', 'ProjectsController@store')->name('store.project');
Route::patch('/projects/{project}/update', 'ProjectsController@update')->name('update.project');
Route::delete('/projects/{project}', 'ProjectsController@destroy')->name('delete.project');

Route::get('/news', 'NewsController@index')->name('news');
Route::get('/news/{id}', 'NewsController@show')->name('show.news');
Route::get('/create', 'NewsController@create')->name('create.news');
Route::post('/news', 'NewsController@store')->name('store.news');
Route::get('/news/{id}/edit', 'NewsController@edit')->name('edit.news');
Route::patch('/news/{id}/update', 'NewsController@update')->name('update.news');
Route::delete('/news/{id}', 'NewsController@destroy')->name('delete.news');

Route::group(['middleware' => 'auth'], function () {
    Route::resource('user', 'UserController', ['except' => ['show']]);
    Route::get('profile/{id}', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
    Route::put('profile/{id}', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
    Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
});

